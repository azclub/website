// pages/_app.tsx
/* eslint-disable react/jsx-props-no-spreading */
import { Container, CssBaseline } from '@mui/material'
import { AppProps } from 'next/app'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useState, useEffect } from 'react'

// import LorikeetGIF from 'assets/lorikeet-no-text.gif'
import Lorikeet from 'assets/lorikeet.webp'
import CookieConsentBanner from 'components/CookieConsent'
import ParrotBG from 'components/ParrotBG'
import Footer from 'containers/Footer'
import Header from 'containers/Header'
import SwitchThemeProvider from 'containers/ThemeProvider'
import '../styles/globals.css'

const MyApp = ({ Component, pageProps }: AppProps) => {
  const [backgroundImage, setBackgroundImage] = useState(Lorikeet.src)

  const router = useRouter()

  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles) {
      jssStyles?.parentElement?.removeChild(jssStyles)
    }

    if (router.pathname !== '/') {
      setBackgroundImage(Lorikeet.src)
    }
  }, [router.pathname])

  return (
    <>
      <Head>
        <title>Parrot Security</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
        <meta name="description" content="Parrot Security website" />
        <link rel="stylesheet" href="https://use.typekit.net/adk3ies.css" />
        <link rel="shortcut icon" href="/favicon.png" />
      </Head>
      <SwitchThemeProvider>
        <CssBaseline />
        <ParrotBG backgroundImage={backgroundImage} />
        <Container maxWidth="xl">
          <Header />
          <Component {...pageProps} />
          <Footer />
        </Container>
        <CookieConsentBanner />
      </SwitchThemeProvider>
    </>
  )
}

export default MyApp
