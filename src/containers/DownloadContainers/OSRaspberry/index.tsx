import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Alert,
  Box,
  Divider,
  Grid,
  Paper,
  Typography
} from '@mui/material'
import { makeStyles } from '@mui/styles'
import Link from 'next/link'
import { useEffect, useState } from 'react'

import PButton from 'components/PButton'
import SelectButton, { SelectButtonItem } from 'components/SelectButton'

const useStyles = makeStyles(theme => ({
  section: {
    marginTop: theme.spacing(8)
  },
  root: {
    width: '100%',
    marginTop: theme.spacing(4),
    padding: theme.spacing(8),
    [theme.breakpoints.down('md')]: {
      padding: theme.spacing(4)
    }
  },
  gridHrMarginTop: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(2)
  },
  subtitleMargined: {
    marginBottom: theme.spacing(4)
  },
  subBlockHeading: {
    fontFamily: 'museo-sans',
    fontWeight: 900
  },
  accordionSummary: {
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  rpiTitle: {
    width: '100%',
    padding: theme.spacing(8),
    [theme.breakpoints.down('md')]: {
      padding: theme.spacing(4)
    }
  },
  docLink: {
    zIndex: 1,
    position: 'relative'
  }
}))

type EditionSelectionProps = {
  initialVersion?: 'core' | 'home' | 'security'
}

type EditionTypes = '' | 'core' | 'home' | 'security'

const OSRaspberry = ({ initialVersion }: EditionSelectionProps) => {
  const classes = useStyles()

  const [edition, setEdition] = useState<EditionTypes>(initialVersion ?? '')

  useEffect(() => setEdition(initialVersion ?? ''), [initialVersion])

  return (
    <>
      <Grid className={classes.section} container justifyContent="center">
        <Paper className={classes.rpiTitle} elevation={0}>
          <Typography variant="h4" paragraph>
            Raspberry Pi Images
          </Typography>
          <Typography variant="subtitle2Semi" paragraph>
            These editions are only compatible with Raspberry Pi devices. Specifically, the
            following images were tested with version 3B, 4B, 400, and 5. The use of a Raspberry Pi
            4/5 is strongly recommended, especially for the Security Edition.
          </Typography>
          <Grid container>
            <Grid item xs={12} container spacing={4} style={{ paddingTop: 30 }}>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" paragraph>
                  Available in the main editions.
                </Typography>
                <Typography variant="body2Semi">Core, Home and Security.</Typography>
              </Grid>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" paragraph>
                  Ready for any context.
                </Typography>
                <Typography variant="body2Semi">
                  Use the full potential of Parrot on your Raspberry Pi.
                </Typography>
              </Grid>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" paragraph>
                  Fully customizable.
                </Typography>
                <Typography variant="body2Semi">
                  You can customize it as you prefer, with any tool.
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Divider variant="fullWidth" className={classes.gridHrMarginTop} />
          </Grid>
          <Grid className={classes.gridHrMarginTop} item xs={12}>
            <Typography className={classes.subBlockHeading} variant="subtitle2" paragraph>
              Minimum Requirements
            </Typography>
            <Grid container item xs={12} justifyContent="space-between" spacing={3}>
              <Grid container direction="column" item xs={12} sm={6} lg={3}>
                <Typography variant="body2Semi">Processor</Typography>
                <Typography variant="body1">ARM</Typography>
              </Grid>
              <Grid container direction="column" item xs={12} sm={6} lg={3}>
                <Typography variant="body2Semi">Graphics</Typography>
                <Typography variant="body1">No Graphical Acceleration Required</Typography>
              </Grid>
              <Grid container direction="column" item xs={12} sm={6} lg={3}>
                <Typography variant="body2Semi">Memory</Typography>
                <Typography variant="body1">512 MB RAM</Typography>
              </Grid>
              <Grid container direction="column" item xs={12} sm={6} lg={3}>
                <Typography variant="body2Semi">Storage</Typography>
                <Typography variant="body1">4 GB available space</Typography>
              </Grid>
            </Grid>
          </Grid>
        </Paper>

        <Paper className={classes.root} elevation={0}>
          <Accordion
            expanded={edition === 'core'}
            onChange={() => setEdition('core')}
            elevation={0}
            style={{ width: '100%', borderRadius: 24 }}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
              style={{ padding: 0 }}
            >
              <Grid container direction="row">
                <Typography variant="h5" paragraph>
                  Core Edition
                </Typography>
                <Typography variant="subtitle2Semi" paragraph>
                  Without DE, you can install whatever you like. Only the base packages are
                  installed in this edition. This makes it the lightest edition of ParrotOS, it
                  should guarantee compatibility with all models of the Raspberry Pi.
                </Typography>
              </Grid>
            </AccordionSummary>
            <AccordionDetails>
              <Grid container spacing={8} justifyContent="center" alignItems="center">
                <Grid container item xs={12} md={8} direction="column">
                  <Alert severity="info" sx={{ fontSize: 18 }}>
                    To proceed with the installation, read{' '}
                    <Link
                      className={classes.docLink}
                      href="https://parrotsec.org/docs/installation/raspberrypi"
                    >
                      <Typography sx={{ textDecoration: 'underline', fontSize: 18 }}>
                        our documentation
                      </Typography>
                    </Link>
                    <Divider sx={{ my: 1 }} light={true} />
                    Default credentials:
                    <Divider sx={{ my: 1 }} />
                    user: <strong>pi</strong>
                    <br />
                    password: <strong>parrot</strong>
                  </Alert>
                </Grid>
                <Grid container item xs={12} md={4} spacing={3} wrap="wrap-reverse">
                  <Grid item xs={12} sm={6} md={12}>
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      paddingTop="10px"
                      paddingBottom="10px"
                    >
                      <Typography variant="body2Semi">Version</Typography>
                      <Typography variant="body2">6.0 Lorikeet</Typography>
                    </Box>
                    <Divider variant="fullWidth" />
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      paddingTop="10px"
                      paddingBottom="10px"
                    >
                      <Typography variant="body2Semi">Release Date</Typography>
                      <Typography variant="body2">Jan 25, 2024</Typography>
                    </Box>
                    <Divider variant="fullWidth" />
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      paddingTop="10px"
                      paddingBottom="10px"
                    >
                      <Typography variant="body2Semi">Architecture</Typography>
                      <Typography variant="body2">arm64</Typography>
                    </Box>
                    <Divider variant="fullWidth" />
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      paddingTop="10px"
                      paddingBottom="10px"
                    >
                      <Typography variant="body2Semi">Size</Typography>
                      <Typography variant="body2">553 MB</Typography>
                    </Box>
                  </Grid>
                  <Grid item xs={12} sm={6} md={12}>
                    <Box display="flex" flexDirection="column" style={{ gap: 10 }}>
                      <SelectButton label="Download" variant="contained">
                        <SelectButtonItem>
                          <Link href="https://deb.parrot.sh/parrot/iso/6.0/Parrot-core-6.0_rpi.img.xz">
                            arm64
                          </Link>
                        </SelectButtonItem>
                      </SelectButton>
                      <SelectButton label="Torrent" variant="outlined">
                        <SelectButtonItem>
                          <Link href="https://deb.parrot.sh/parrot/iso/6.0/Parrot-core-6.0_rpi.img.xz.torrent">
                            arm64
                          </Link>
                        </SelectButtonItem>
                      </SelectButton>
                      <PButton
                        variant="outlined"
                        to="https://deb.parrot.sh/parrot/iso/6.0/signed-hashes.txt"
                      >
                        Check Hashes
                      </PButton>
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
            </AccordionDetails>
          </Accordion>
        </Paper>
        <Paper className={classes.root} elevation={0}>
          <Accordion
            expanded={edition === 'home'}
            onChange={() => setEdition('home')}
            elevation={0}
            style={{ width: '100%', borderRadius: 24 }}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
              style={{ padding: 0, marginTop: 0 }}
            >
              <Grid container direction="row">
                <Typography variant="h5" paragraph>
                  Home Edition
                </Typography>
                <Typography variant="subtitle2Semi" paragraph>
                  This version of Parrot is a lightweight installation which provides the essential
                  tools needed to start working.
                </Typography>
              </Grid>
            </AccordionSummary>
            <AccordionDetails style={{ padding: 0 }}>
              <Grid container spacing={8} justifyContent="center" alignItems="center">
                <Grid container item xs={12} md={8} direction="column">
                  <Alert severity="warning" sx={{ my: 2, fontSize: 18 }}>
                    This image may run on older Raspberry Pi versions, but Raspberry Pi 4 or greater
                    with at least 1 GB of RAM is recommended.
                  </Alert>
                  <Alert severity="info" sx={{ fontSize: 18 }}>
                    To proceed with the installation, read{' '}
                    <Link
                      className={classes.docLink}
                      href="https://parrotsec.org/docs/installation/raspberrypi"
                    >
                      <Typography sx={{ textDecoration: 'underline', fontSize: 18 }}>
                        our documentation
                      </Typography>
                    </Link>
                    <Divider sx={{ my: 1 }} light={true} />
                    Default credentials:
                    <Divider sx={{ my: 1 }} />
                    user: <strong>pi</strong>
                    <br />
                    password: <strong>parrot</strong>
                  </Alert>
                </Grid>
                <Grid container item xs={12} md={4} spacing={3} wrap="wrap-reverse">
                  <Grid item xs={12} sm={6} md={12}>
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      paddingTop="10px"
                      paddingBottom="10px"
                    >
                      <Typography variant="body2Semi">Version</Typography>
                      <Typography variant="body2">6.0 Lorikeet</Typography>
                    </Box>
                    <Divider variant="fullWidth" />
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      paddingTop="10px"
                      paddingBottom="10px"
                    >
                      <Typography variant="body2Semi">Release Date</Typography>
                      <Typography variant="body2">Jan 25, 2024</Typography>
                    </Box>
                    <Divider variant="fullWidth" />
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      paddingTop="10px"
                      paddingBottom="10px"
                    >
                      <Typography variant="body2Semi">Architecture</Typography>
                      <Typography variant="body2">arm64</Typography>
                    </Box>
                    <Divider variant="fullWidth" />
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      paddingTop="10px"
                      paddingBottom="10px"
                    >
                      <Typography variant="body2Semi">Size</Typography>
                      <Typography variant="body2">1.2 GB</Typography>
                    </Box>
                  </Grid>
                  <Grid item xs={12} sm={6} md={12}>
                    <Box display="flex" flexDirection="column" style={{ gap: 10 }}>
                      <SelectButton label="Download" variant="contained">
                        <SelectButtonItem>
                          <Link href="https://deb.parrot.sh/parrot/iso/6.0/Parrot-home-6.0_rpi.img.xz">
                            arm64
                          </Link>
                        </SelectButtonItem>
                      </SelectButton>
                      <SelectButton label="Torrent" variant="outlined">
                        <SelectButtonItem>
                          <Link href="https://deb.parrot.sh/parrot/iso/6.0/Parrot-home-6.0_rpi.img.xz.torrent">
                            arm64
                          </Link>
                        </SelectButtonItem>
                      </SelectButton>
                      <PButton
                        variant="outlined"
                        to="https://deb.parrot.sh/parrot/iso/6.0/signed-hashes.txt"
                      >
                        Check Hashes
                      </PButton>
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
            </AccordionDetails>
          </Accordion>
        </Paper>
        <Paper className={classes.root} elevation={0}>
          <Accordion
            expanded={edition === 'security'}
            onChange={() => setEdition('security')}
            elevation={0}
            style={{ width: '100%', borderRadius: 24 }}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
              style={{ padding: 0, marginTop: 0 }}
            >
              <Grid container direction="row">
                <Typography variant="h5" paragraph>
                  Security Edition
                </Typography>
                <Typography variant="subtitle2Semi" paragraph>
                  As the name suggests, this is the full edition. After the installation you have a
                  complete out of the box pentesting workstation loaded with a large variety of
                  tools ready to use.
                </Typography>
              </Grid>
            </AccordionSummary>
            <AccordionDetails>
              <Grid container spacing={8} justifyContent="center" alignItems="center">
                <Grid container item xs={12} md={8} direction="column">
                  <Alert severity="warning" sx={{ my: 2, fontSize: 18 }}>
                    This image may run on older Raspberry Pi versions, but Raspberry Pi 4 or greater
                    with at least 2 GB of RAM is recommended.
                  </Alert>
                  <Alert severity="info" sx={{ fontSize: 18 }}>
                    To proceed with the installation, read{' '}
                    <Link
                      className={classes.docLink}
                      href="https://parrotsec.org/docs/installation/raspberrypi"
                    >
                      <Typography sx={{ textDecoration: 'underline', fontSize: 18 }}>
                        our documentation
                      </Typography>
                    </Link>
                    <Divider sx={{ my: 1 }} light={true} />
                    Default credentials:
                    <Divider sx={{ my: 1 }} />
                    user: <strong>pi</strong>
                    <br />
                    password: <strong>parrot</strong>
                  </Alert>
                </Grid>
                <Grid container item xs={12} md={4} spacing={3} wrap="wrap-reverse">
                  <Grid item xs={12} sm={6} md={12}>
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      paddingTop="10px"
                      paddingBottom="10px"
                    >
                      <Typography variant="body2Semi">Version</Typography>
                      <Typography variant="body2">6.0 Lorikeet</Typography>
                    </Box>
                    <Divider variant="fullWidth" />
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      paddingTop="10px"
                      paddingBottom="10px"
                    >
                      <Typography variant="body2Semi">Release Date</Typography>
                      <Typography variant="body2">Jan 25, 2024</Typography>
                    </Box>
                    <Divider variant="fullWidth" />
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      paddingTop="10px"
                      paddingBottom="10px"
                    >
                      <Typography variant="body2Semi">Architecture</Typography>
                      <Typography variant="body2">arm64</Typography>
                    </Box>
                    <Divider variant="fullWidth" />
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      paddingTop="10px"
                      paddingBottom="10px"
                    >
                      <Typography variant="body2Semi">Size</Typography>
                      <Typography variant="body2">3.1 GB</Typography>
                    </Box>
                  </Grid>
                  <Grid item xs={12} sm={6} md={12}>
                    <Box display="flex" flexDirection="column" style={{ gap: 10 }}>
                      <SelectButton label="Download" variant="contained">
                        <SelectButtonItem>
                          <Link href="https://deb.parrot.sh/parrot/iso/6.0/Parrot-security-6.0_rpi.img.xz">
                            arm64
                          </Link>
                        </SelectButtonItem>
                      </SelectButton>
                      <SelectButton label="Torrent" variant="outlined">
                        <SelectButtonItem>
                          <Link href="https://deb.parrot.sh/parrot/iso/6.0/Parrot-security-6.0_rpi.img.xz.torrent">
                            arm64
                          </Link>
                        </SelectButtonItem>
                      </SelectButton>
                      <PButton
                        variant="outlined"
                        to="https://deb.parrot.sh/parrot/iso/6.0/signed-hashes.txt"
                      >
                        Check Hashes
                      </PButton>
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
            </AccordionDetails>
          </Accordion>
        </Paper>
      </Grid>
    </>
  )
}

export default OSRaspberry
